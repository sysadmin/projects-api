// SPDX-FileCopyrightText: 2017-2020 Harald Sitter <sitter@kde.org>
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

package apis

import (
	"errors"
	"net/http"
	"testing"

	"invent.kde.org/sysadmin/projects-api.git/models"
)

// Test Double
type ProjectService struct {
}

func NewProjectService() *ProjectService {
	return &ProjectService{}
}

func (s *ProjectService) Get(path string) (*models.Project, error) {
	project := models.Project{}
	if path == "calligra/krita" {
		project.PathOnDisk = "calligra/krita"
		project.Repo = "krita"
		project.Identifier = "kritaid"
		return &project, nil
	}
	if path == "calligra" {
		return nil, nil
	}
	if path == "mammoth-with-lts" {
		project.I18n = models.I18nData{
			TrunkKF5:     "master",
			StableKF5:    "other",
			StableLTSKF5: "older",
			TrunkKF6:     "kf6-trunk",
			StableKF6:    "kf6-stable",
			StableLTSKF6: "kf6-lts",
		}
		project.Bugzilla = models.BugzillaData{
			Product:   "mammoth with lts",
			Component: "test",
		}
		name := "mammoth-with-lts"
		project.PathOnDisk = name
		project.Repo = name
		project.Identifier = name
		project.Active = true
		project.BugDatabase = "https://bugs.kde.org/db"
		project.BugSubmit = "https://bugs.kde.org/submit"
		return &project, nil
	}
	return &project, errors.New("unexpected path " + path)
}

func (s *ProjectService) GetByIdentifier(id string) (*models.Project, error) {
	project := models.Project{}
	if id == "kritaid" {
		project.PathOnDisk = "calligra/krita"
		project.Repo = "krita"
		project.Identifier = "kritaid"
		return &project, nil
	}
	if id == "foo" {
		return nil, nil
	}
	return &project, errors.New("unexpected id " + id)
}

func (s *ProjectService) Find(filter *models.ProjectFilter) ([]string, error) {
	projects := []string{"calligra/krita"}
	if filter.Basename == "krita" && filter.RepoPath == "" {
		return projects, nil
	}
	if filter.Basename == "" && filter.RepoPath == "krita" {
		return projects, nil
	}
	if filter.Basename == "" && filter.InactiveOnly {
		return []string{"kde/hole"}, nil
	}
	if filter.Basename == "" && filter.RepoPath == "" {
		return append(projects, "frameworks/solid"), nil
	}
	panic("unexpected query " + filter.Basename + " " + filter.RepoPath)
}

func (s *ProjectService) List(path string) ([]string, error) {
	if path == "" {
		return []string{"calligra/krita", "frameworks/solid"}, nil
	}
	if path == "calligra" {
		return []string{"calligra/krita"}, nil
	}
	panic("unexpected query " + path)
}

func (s *ProjectService) Identifiers(filter *models.ProjectFilter) ([]string, error) {
	return []string{"kritaid"}, nil
}

func init() {
	v1 := router.Group("/v1")
	{
		ServeProjectResource(v1, NewProjectService())
	}
}

func TestProject(t *testing.T) {
	kritaObj := `{"bug_database":"", "bug_submit":"", "i18n":{"component":"", "stable":"", "stableKF5":"", "trunk":"", "trunkKF5":""}, "identifier":"kritaid", "path":"calligra/krita", "repo":"krita"}`
	mammothWithLtsObj := `{"i18n":{"component":"", "stable":"", "stableKF5":"other", "stableKF6":"kf6-stable", "stableLTSKF5":"older", "stableLTSKF6":"kf6-lts", "trunk":"", "trunkKF5":"master", "trunkKF6":"kf6-trunk"}, "identifier":"mammoth-with-lts", "path":"mammoth-with-lts", "repo":"mammoth-with-lts", "bug_database":"https://bugs.kde.org/db", "bug_submit":"https://bugs.kde.org/submit"}`
	runAPITests(t, []apiTestCase{
		{"get a project", "GET", "/v1/project/calligra/krita", "", http.StatusOK, kritaObj},
		{"get nil project", "GET", "/v1/project/calligra", "", http.StatusNotFound, ""},
		{"get a project with i18n", "GET", "/v1/project/mammoth-with-lts", "", http.StatusOK, mammothWithLtsObj},
		{"get by id", "GET", "/v1/identifier/kritaid", "", http.StatusOK, kritaObj},
		{"get by bad id", "GET", "/v1/identifier/foo", "", http.StatusNotFound, ""},
		{"get by repopath", "GET", "/v1/repo/krita", "", http.StatusOK, kritaObj},
		{"find by id", "GET", "/v1/find?id=krita", "", http.StatusOK, `["calligra/krita"]`},
		{"find by repopath", "GET", "/v1/find?repo=krita", "", http.StatusOK, `["calligra/krita"]`},
		{"find by only inactive", "GET", "/v1/find?inactive=true", "", http.StatusOK, `["kde/hole"]`},
		{"find conflicting basename and id", "GET", "/v1/find?id=a&basename=b", "", http.StatusConflict, ""},
		{"find all", "GET", "/v1/find", "", http.StatusOK, `["calligra/krita", "frameworks/solid"]`},
		{"list", "GET", "/v1/projects", "", http.StatusOK, `["calligra/krita", "frameworks/solid"]`},
		{"list component", "GET", "/v1/projects/calligra", "", http.StatusOK, `["calligra/krita"]`},
		{"list identifiers", "GET", "/v1/identifiers", "", http.StatusOK, `["kritaid"]`},
	})
}
