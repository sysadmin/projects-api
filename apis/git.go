// SPDX-FileCopyrightText: 2017 Harald Sitter <sitter@kde.org>
// SPDX-License-Identifier: AGPL-3.0-or-later

package apis

import (
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

type gitService interface {
	UpdateClone() string
	Age() time.Duration
}

type gitResource struct {
	service gitService
}

// ServeGitResource creates a new API resource.
func ServeGitResource(rg *gin.RouterGroup, service gitService) {
	r := &gitResource{service}
	rg.GET("/poll", r.poll)
}

/**
 * @api {get} /poll Update Clone
 *
 * @apiVersion 1.0.0
 * @apiGroup Project
 * @apiName poll
 *
 * @apiDescription Updates internal repo-metadata clone. Generally not necessary
 *   to call as the clone is updated automatically. This endpoint is handy when
 *   forcing an update is called for. This is subject to rate limiting.
 *
 * @apiSuccessExample {json} Success-Response:
 *   "Alread up-to-date."
 * @apiError {json} TooManyRequests Already up to date enough.
 */
func (r *gitResource) poll(c *gin.Context) {
	since := r.service.Age()
	if since.Minutes() < 2.0 {
		c.JSON(http.StatusTooManyRequests,
			fmt.Sprintf("Not updating. Last update was %s ago.", since))
		return
	}
	c.JSON(http.StatusOK, r.service.UpdateClone())
}
