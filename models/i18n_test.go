// SPDX-FileCopyrightText: 2017-2023 Harald Sitter <sitter@kde.org>
// SPDX-License-Identifier: AGPL-3.0-or-later

package models

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestInferIdentifier(t *testing.T) {
	// Not actually an inference. If the Project has an Identifier set, it's the
	// component since the gitlab transition.
	i := I18nData{}
	p := Project{
		PathOnDisk: "kdesupport/phonon",
		Repo:       "kdesupport/phonon",
		Identifier: "foobar",
	}
	i.Infer(&p)
	assert.Equal(t, "foobar", i.Component)
}

func TestUnmarshalI18n(t *testing.T) {
	// Make sure all well-known fields are read into the struct
	i := I18nData{}
	i.UnmarshalJSON([]byte(`{"trunk":"tr", "trunk_kf5":"tr5", "trunk_kf6":"tr6", "stable":"st", "stable_kf5":"st5", "stable_kf6":"st6", "stable_lts_kf5":"lts5", "stable_lts_kf6":"lts6"}`))
	assert.Equal(t, "st", i.Stable)
	assert.Equal(t, "st5", i.StableKF5)
	assert.Equal(t, "st6", i.StableKF6)
	assert.Equal(t, "lts5", i.StableLTSKF5)
	assert.Equal(t, "lts6", i.StableLTSKF6)
	assert.Equal(t, "tr", i.Trunk)
	assert.Equal(t, "tr5", i.TrunkKF5)
	assert.Equal(t, "tr6", i.TrunkKF6)
}

func TestUnmarshalI18nMissingFields(t *testing.T) {
	// Don't fall over if a field doesn't exist
	i := I18nData{}
	i.UnmarshalJSON([]byte(`{"trunk":"tr"}`))
	assert.Equal(t, "tr", i.Trunk)
	assert.Equal(t, "", i.TrunkKF5)
	assert.Equal(t, "", i.TrunkKF6)
}

func TestMerge(t *testing.T) {
	// Merge correctly
	i := I18nData{}
	other := I18nData{Stable: "st", Trunk: "tr", StableKF5: "st5", TrunkKF5: "tr5", StableLTSKF5: "lts5", TrunkKF6: "tr6", StableKF6: "st6", StableLTSKF6: "lts6"}
	i.Merge(other)
	assert.Equal(t, "st", i.Stable)
	assert.Equal(t, "st5", i.StableKF5)
	assert.Equal(t, "st6", i.StableKF6)
	assert.Equal(t, "lts5", i.StableLTSKF5)
	assert.Equal(t, "lts6", i.StableLTSKF6)
	assert.Equal(t, "tr", i.Trunk)
	assert.Equal(t, "tr5", i.TrunkKF5)
	assert.Equal(t, "tr6", i.TrunkKF6)
}

func TestMergeEmpty(t *testing.T) {
	// When a value is empty in other it shouldn't override.
	i := I18nData{StableKF6: "st6"}
	other := I18nData{StableKF6: ""}
	i.Merge(other)
	assert.Equal(t, "st6", i.StableKF6)
}
