// SPDX-FileCopyrightText: 2017 Harald Sitter <sitter@kde.org>
// SPDX-License-Identifier: AGPL-3.0-or-later

package services

import (
	"time"

	"invent.kde.org/sysadmin/projects-api.git/models"
)

type gitDAO interface {
	UpdateClone() string
	Age() time.Duration
	Get(path string) (*models.Project, error)
	List() []string
}

type GitService struct {
	dao gitDAO
}

func NewGitService(dao gitDAO) *GitService {
	return &GitService{dao}
}

func (s *GitService) UpdateClone() string {
	return s.dao.UpdateClone()
}

func (s *GitService) Age() time.Duration {
	return s.dao.Age()
}
