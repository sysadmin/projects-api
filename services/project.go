// SPDX-FileCopyrightText: 2017-2020 Harald Sitter <sitter@kde.org>
// SPDX-License-Identifier: AGPL-3.0-or-later

package services

import (
	"path/filepath"
	"strings"

	"invent.kde.org/sysadmin/projects-api.git/models"
)

type ProjectService struct {
	dao gitDAO
}

func NewProjectService(dao gitDAO) *ProjectService {
	return &ProjectService{dao}
}

func (s *ProjectService) Get(path string) (*models.Project, error) {
	return s.dao.Get(path)
}

func (s *ProjectService) GetByIdentifier(id string) (*models.Project, error) {
	for _, path := range s.dao.List() {
		if !s.isProject(path) {
			continue
		}

		model, _ := s.Get(path)
		if id == model.Identifier {
			return model, nil
		}
	}

	return nil, nil
}

func (s *ProjectService) isProject(path string) bool {
	model, err := s.Get(path)
	if err != nil {
		panic(err)
	}
	return model != nil && model.Repo != ""
}

func (s *ProjectService) filterProjects(filter *models.ProjectFilter) ([]*models.Project, error) {
	matches := []*models.Project{}

	for _, path := range s.dao.List() {
		if !s.isProject(path) {
			continue
		}

		if len(filter.Basename) != 0 && filter.Basename != filepath.Base(path) {
			continue
		}
		model, _ := s.Get(path)
		if len(filter.RepoPath) != 0 {
			if model.Repo != filter.RepoPath {
				continue // doesn't match repopath constraint
			}
		}
		if filter.ActiveOnly && !model.Active {
			continue
		}
		if filter.InactiveOnly && model.Active {
			continue
		}
		if filter.AnyI18n {
			i18n := model.I18n
			if i18n.Stable == "none" && i18n.Trunk == "none" &&
				i18n.StableKF5 == "none" && i18n.TrunkKF5 == "none" &&
				i18n.StableKF6 == "none" && i18n.TrunkKF6 == "none" {
				continue
			}
		}

		matches = append(matches, model)
	}

	return matches, nil
}

func (s *ProjectService) Find(filter *models.ProjectFilter) ([]string, error) {
	matches := []string{}

	projects, err := s.filterProjects(filter)
	if err != nil {
		return matches, err
	}

	for _, project := range projects {
		matches = append(matches, project.PathOnDisk)
	}
	return matches, nil
}

func (s *ProjectService) List(prefix string) ([]string, error) {
	matches := []string{}

	for _, path := range s.dao.List() {
		if !s.isProject(path) {
			continue
		}

		if len(prefix) == 0 || path == prefix || strings.HasPrefix(path, prefix+"/") {
			matches = append(matches, path)
		}
	}

	return matches, nil
}

func (s *ProjectService) Identifiers(filter *models.ProjectFilter) ([]string, error) {
	matches := []string{}

	projects, err := s.filterProjects(filter)
	if err != nil {
		return matches, err
	}

	for _, project := range projects {
		matches = append(matches, project.Identifier)
	}
	return matches, nil
}
