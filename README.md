<!--
SPDX-License-Identifier: CC0-1.0
SPDX-FileCopyrightText: 2017-2020 Harald Sitter <sitter@kde.org>
-->

# Requirements

Needs Go 1.8 if one intends to run tests (which one should of course).

# Deployment

Deployment is done via `make deploy` which builds locally and pushes relevant artifacts to server.

# Documentation

Documentation uses apidocjs.com. Run `npm install apidoc` to be able to `make doc`.
Requests to / are automatically redirected to /doc/ where the doc directory will
be served.

# Code Structure

- `main.go`
- `apis/` contains API structs, the structs setup their routes into a Gin routing group
- `services/` API logic, given the simplicity of the API this is mostly just passthru
- `daos/` data access objects meddling with the on-disk data and marshal/demarshal thereof
- `models/` data structs exchaned between the apis/services/daos

Note that all parts are dependency inverted, i.e. they get their closest partner
from main.go rather than themselves being aware of their partner. While this is
more code it allows for simpler unit testing on each level as you can insert a
test-double by implementing the relevant interface.
